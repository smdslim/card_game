var Paddle = window.Paddle || {},
    Events = window.Events || {},
    Game = window.Game || {};

Paddle = (function () {
    var player = {
            cards: {},
            life: 60,
            mana: {
                fire: 6,
                water: 8,
                air: 8,
                earth: 8,
                spirit: 8
            },
            current_turn: true
        },
        enemy = {
            cards: {},
            life: 60,
            mana: {
                fire: 7,
                water: 8,
                air: 8,
                earth: 8,
                spirit: 8
            },
            current_turn: false
        },
        player_indexes = [],
        enemy_indexes = [],
        player_turn = null;

    function spread_the_cards(data) {
        var basic_groups = [
                [0, 1, 2],
                [3, 4, 5],
                [6, 7, 8],
                [9, 10, 11]
            ],
            add_groups = [
                [0, 1],
                [2, 3],
                [4, 5],
                [6, 7]
            ], temp;
        player = player.cards,
            enemy = enemy.cards;

        for (var school in data) {
            if (player[school] == undefined) player[school] = [];
            if (enemy[school] == undefined) enemy[school] = [];


            temp = $.extend(true, [], basic_groups);
            if (['fire', 'water', 'air', 'earth'].indexOf(school) == -1) {
                temp = $.extend(true, [], add_groups);
            }

            player_indexes = build_indexes(temp);
            enemy_indexes = build_indexes(temp);

            player_indexes.forEach(function (item) {
                player[school].push(data[school][item]);
            });
            enemy_indexes.forEach(function (item) {
                enemy[school].push(data[school][item]);
            });
        }

    }


    function build_indexes(a) {
        var index, result = [];
        a.forEach(function (item, i) {
            index = Math.floor(Math.random() * item.length);
            result.push(a[i].splice(index, 1)[0]);
        })
        return result;
    }

    function roll_dice() {
        return !!Math.floor(Math.random() * 2);
    }

    function update_cards_data(person) {
        var cards = person.cards,
            mana = person.mana,
            _card;

        for (var school in cards) {
            for (var key in cards[school]) {
                _card = cards[school][key];
                if (_card.cost <= mana[school]) {
                    _card.active = true;
                } else {
                    _card.active = false;
                }
            }
        }

    }

    return {
        spread_the_cards: spread_the_cards,
        player: player,
        enemy: enemy,
        roll_dice: roll_dice,
        player_turn: player_turn,
        update_cards_data: update_cards_data
    }
})();

Events = (function () {

    var list = {
        card_played: "card_played"
    };

    function cardPlayed() {
        $(document).trigger(list.card_played);
    }

    return {
        cardPlayed: cardPlayed,
        list: list
    }
})();

Game = (function () {
    var player_turn = true;
})();





