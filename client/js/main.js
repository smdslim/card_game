$(function () {
    Paddle.spread_the_cards(cards);
    Paddle.enemy.current_turn = !Paddle.roll_dice();
    Paddle.player.current_turn = !Paddle.enemy.current_turn;

    // dropping card on the battlefield
    $('.b-player-cards .b-card').droppable({
        hoverClass: "ui-state-hover",
        drop: function (event, ui) {

            // возвращаем false если слот для карты уже занят
            if ($(this).find('.card-image').length != 0) return false;

            var $card = ui.draggable.clone(),
                attack = $card.attr('data-attack'),
                life = $card.attr('data-life'),
                cost = $card.attr('data-cost'),
                school = $card.attr('data-school');

            $(this).html('');
            $(this).append('<div class="attack">' + attack + '</div>');
            $(this).append('<div class="cost">' + cost + '</div>');
            $(this).append('<div class="life">' + life + '</div>');
            $(this).append($card);

            //debugger;
            Paddle.player.mana[school] -= cost;
            render_cards();

            // triggering card_played event
            Events.cardPlayed();
        }
    });

    // using spell
    $(document).on('click', '.spell', function () {
        var cost = $(this).attr('data-cost'),
            school = $(this).attr('data-school');

        Paddle.player.mana[school] -= cost;
        render_cards();

        // triggering card_played event
        Events.cardPlayed();
    });

    render_cards();


});


function render_cards() {

    Paddle.update_cards_data(Paddle.enemy);
    Paddle.update_cards_data(Paddle.player);


    $(".player-cards .creature, .player-cards .spell, .block-div").remove();

    $('.player-cards .col').each(function () {
        var $col = $(this),
            school = $col.attr('data');

        if (school != undefined) {

            // простановка маны
            $('.enemy-mana').find('span.school-name[data=' + school + ']').next().html(Paddle.enemy.mana[school]);
            $('.player-mana').find('span.school-name[data=' + school + ']').next().html(Paddle.player.mana[school]);

            // расстановка карт
            $col.find('.p-card').each(function (index) {
                var $card = $(this), $img,
                    attack = Paddle.player.cards[school][index].attack,
                    cost = Paddle.player.cards[school][index].cost,
                    life = Paddle.player.cards[school][index].life,
                    active = Paddle.player.cards[school][index].active;

                $img = $('<img class="card-image creature" src="img/cards/' + Paddle.player.cards[school][index].image + '">');
                $img.attr({
                    'data-attack': attack,
                    'data-life': life,
                    'data-cost': cost,
                    'data-school': school
                });

                // если карта не заклинание то добавляем возможность перетаскивать
                if (life != 0 && active) {
                    $img.draggable({
                        helper: "clone",
                        appendTo: "body",
                        drag: function (event, ui) {
                            //console.log(event.type);
                        },
                        start: function () {

                        }
                    });
                }

                if (life == 0) {
                    $img.removeClass('creature');
                    $img.addClass('spell');
                    $img[0].draggable = false;
                }

                // создание блокирующего дива если маны не достаточно
                if (!active) {
                    var $div = $("<div/>");

                    $div.attr({class: 'block-div'});
                    $card.append($div);
                }


                $card.append('<div class="attack">' + attack + '</div>');
                $card.append('<div class="cost">' + cost + '</div>');
                $card.append('<div class="life">' + life + '</div>');
                $card.append($img);
            });
        }
    });
}

// Global events listening

(function () {
    $(document).on(Events.list.card_played, function () {
        console.log("Card played");
    });
})();