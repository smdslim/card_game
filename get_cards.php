<?php

require_once 'vendor/autoload.php';


use Sunra\PhpSimple\HtmlDomParser;

$url  = "http://www.spectromancer.com/cardlist_ru.htm";
$html = file_get_contents( $url );

$dom = HtmlDomParser::str_get_html( $html );
// $table = $dom->find('table', 0)->find();

// $card_name = $table->children(2)->find('.cardname', 0)->innertext;
// $card_image = 'http://www.spectromancer.com/' . $table->children(0)->find('.card', 0)->src;


//print_r([$card_image, $card_name]);


$str = "sep=;\r\n";

foreach ( $dom->find( '.cardname' ) as $card ) {

	$card_name        = $card->innertext;
	$card_description = str_replace( "\r\n", ", ", $card->parent()->plaintext );
	$card_description = str_replace( $card_name, '', $card_description );
	$card_cost        = $card->parent()->prev_sibling()->children( 0 )->innertext;
	$card_image_el    = $card->parent()->prev_sibling()->prev_sibling()->find( '.card', 0 );
	if ( count( $card_image_el ) == 0 ) {
		$card_image_el = $card->parent()->prev_sibling()->prev_sibling()->find( '.spell', 0 );
	}
	$card_image     = str_replace( 'img/cards/', '', $card_image_el->src );
	$img = file_get_contents('http://spectromancer.com/' . $card_image_el->src);
	file_put_contents('images/' . $card_image, $img);
	$card_attack_el = $card->parent()->prev_sibling()->prev_sibling()->find( '.attack', 0 );
	$card_life_el   = $card->parent()->prev_sibling()->prev_sibling()->find( '.life', 0 );
	$card_attack    = '';
	$card_life      = '';
	if ( count( $card_attack_el ) == 1 ) {
		$card_attack = $card_attack_el->innertext;
	}
	if ( count( $card_life_el ) == 1 ) {
		$card_life = $card_life_el->innertext;
	}
	$card_type = $card->parent()->parent()->parent()->prev_sibling()->innertext;

	$str .= $card_name . ';' . $card_description . ';' . $card_cost . ';' . $card_attack . ';' . $card_life . ';' . $card_type . ';' . $card_image . "\r\n";

}

	file_put_contents('cards.csv', $str);



